import React, { Component } from 'react';
import './board.styles.css'
import Row from "./row.component";

const numRows = 10, numCols = 10;


class Board extends Component {


     generateRows = () => {
        let rows = [];
        for (let rowId = 1; rowId <= numRows; rowId++) {
            rows.push(<Row rowId={rowId} numCols={numCols} key={"row" + rowId}/>);
        }
        return rows;
    };

    render() {
        return (
            <div className="Board">
                {this.generateRows()}
            </div>
        );
    }
}

export default Board;